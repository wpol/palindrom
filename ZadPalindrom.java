public class ZadPalindrom {
    public static void main(String[] args) {
        System.out.println(isPalindrom("kobyla Ma maly bok"));

    }

    private static boolean isPalindrom(String sentence) {
        String temp = withoutSpaces(sentence);
        return isPalindromWithoutSpaces(temp);
    }

    private static boolean isPalindromWithoutSpaces(String toCheck) {
        if (toCheck.length() <= 1) {
            return true;
        }
        if (toCheck.charAt(0) != toCheck.charAt(toCheck.length() - 1)) {
            return false;
        }
        return isPalindrom(withoutBounds(toCheck));
    }

    private static String withoutSpaces(String exp) {
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < exp.length(); i++) {
            if (exp.charAt(i) != ' ') {
                temp.append(exp.charAt(i));
            }
        }
        return temp.toString().toUpperCase();
    }

    private static String withoutBounds(String expr) {
        return expr.substring(1, expr.length() - 1);
    }
}
